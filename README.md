#FirstProject

FirstProject is a Java application which contains logic for a hero game. This game was made during my time at Experis Academy as a assignment. The main purpose with this game is that you can create a hero with some requierd fields, and once you have a hero you should be able to get a weapon and/or armor with the same level as your hero. You should also be able to level up, and once you level up you should be able to get a weapon and/or armor with the same level as your hero

##Installation

No further installations for this application. The application is written in Java version 18, so make sure you have Java installed in your code editor.

##Usage

#SetWeapon
Sets a weapon for hero. BUT, depends on what hero you choose to be, you can only get a certain type of weapon. You most also have the same or higher level than the weapon to get it. Once created weapon, it will be stored in your own hashmap.If you change weapon, your current weapon will be replaced with the new one

#SetArmor
Sets a armor for your hero. BUT, what hero you choose to be, you can only equip certain types of armor. You most also have the same or higher level as the armor.

##Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change. Please make sure to update tests as appropriate.


