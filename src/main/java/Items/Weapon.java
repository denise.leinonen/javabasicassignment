package Items;

public class Weapon extends Item {
    private int damage;
    private double attackSpeed;

    private WeaponType weaponType;

    private double weaponDPS;

    public Weapon(String name, int level, Slots slots, int damage, double attackSpeed, WeaponType weaponType) {
        super(name, level, slots);
        this.damage = damage;
        this.attackSpeed = attackSpeed;
        this.weaponType = weaponType;
        this.weaponDPS = damage * attackSpeed;
    }

    public int getDamage() {
        return damage;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }


    public double getWeaponDPS() {
        return weaponDPS;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    @Override
    public String toString() {
        return " Weapon:" + "\n" +
                "damage: " + damage + "\n" +
                "attackSpeed: " + attackSpeed + "\n" +
                "weaponType: " + weaponType;
    }
}
