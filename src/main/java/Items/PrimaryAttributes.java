package Items;

public class PrimaryAttributes {
    private int strength;
    private int dexterity;
    private int intelligence;

    public PrimaryAttributes(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    @Override
    public String toString() {
        return "PrimaryAttributes: " + "\n" +
                "strength: " + strength + "\n" +
                "dexterity: " + dexterity + "\n" +
                "intelligence: " + intelligence;
    }
}
