package Items;

import Items.Item;

public class Armor extends Item {
    private ArmorType armorType;

    private PrimaryAttributes primaryAttributes;
    public Armor(String name, int level, Slots slots, ArmorType armorType, PrimaryAttributes primaryAttributes) {
        super(name, level, slots);
        this.armorType = armorType;
        this.primaryAttributes = primaryAttributes;
    }

    public PrimaryAttributes getPrimaryAttributes() {
        return primaryAttributes;
    }

    public PrimaryAttributes setPrimaryAttributes(PrimaryAttributes primaryAttributes) {
        this.primaryAttributes = primaryAttributes;
        return primaryAttributes;
    }

    public ArmorType getArmorType() {
        return armorType;
    }

    @Override
    public String toString() {
        return "Armor: " + "\n" +
                "armorType:" + armorType + "\n" +
                "primaryAttributes:" + primaryAttributes;
    }
}
