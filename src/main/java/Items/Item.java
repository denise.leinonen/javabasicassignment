package Items;

public class Item {

    private String name;
    private int level;

    private Slots slots;

    public Item(String name, int level, Slots slots) {
        this.name = name;
        this.level = level;
        this.slots = slots;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public Slots getSlots() {
        return slots;
    }

}
