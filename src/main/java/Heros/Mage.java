package Heros;

import Exceptions.InvalidArmorException;
import Exceptions.InvalidSlotException;
import Exceptions.InvalidWeaponException;

import Items.*;

public class Mage extends Hero {

    private Weapon weapon;

    private Armor armor;

    public Mage(String name) {
        super(name, 1,  new PrimaryAttributes(1, 1, 8));
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public Armor getArmor() {
        return armor;
    }
    //Sets a weapon for the mage, but first it must pass all if statements
    @Override
    public Boolean setWeapon(Weapon weapon) throws Exception {
        //Declaring some variables for the user input
        WeaponType weaponType = weapon.getWeaponType();
        int weaponLevel = weapon.getLevel();
        Slots slots = weapon.getSlots();
        //a mage can only equip STAFF or WAND as weapon. If it tries to equip another
        // weapon than that it throws InvalidWeaponException
        if (!(weaponType.equals(WeaponType.STAFF) ||
                weaponType.equals(WeaponType.WAND))) {
            throw new InvalidWeaponException("You can not get that weapon");
        }
        //If Mage tries to equip weapon with higher level than the mage has it will throw
        // InvalidWeaponException
        if (weaponLevel > getLevel()) {
            throw new InvalidWeaponException("You can't get a weapon with that level");
        }
        //If the Mage tries to enter a slot that isn't weapon it throws InvalidSlotException
        if (!slots.equals(Slots.WEAPON)) {
            throw new InvalidSlotException("You must put WEAPON as slot");
        } else {
            //If the mage try to set weapon with right slot, weaponType and level it will get that
            //Weapon
            // ,and it will also put the weapon in mages hashmap
            this.weapon = weapon;
            putSlotsAndWeaponInHashmap();
            return true;
        }
    }

    //Sets armor for the mage, but first it must pass all if statements
    @Override
    public Boolean setArmor(Armor armor) throws InvalidArmorException, InvalidSlotException {
        //Declaring some variables for the user input
        ArmorType armorType = armor.getArmorType();
        int armorLevel = armor.getLevel();
        Slots slots = armor.getSlots();
        //Mage can only equip armor with type CLOTH, so if mage tries to equip
        //another type than that it will throw InvalidArmorException
        //Armor can't be put in weapon slot, if hero tries to do it exception will throw
        if(slots.equals(Slots.WEAPON)){
            throw new InvalidSlotException("You can't put armor in WEAPON slot!");
        }
        if (!armorType.equals(ArmorType.CLOTH)) {
            throw new InvalidArmorException("You can not get that armor");
        }
        //If Mage tries to equip armor with higher level than the mage has it will throw
        //InvalidArmorException
        if (armorLevel > getLevel()) {
            throw new InvalidArmorException("You can't get armor with that level");
        } else {
            //If mage tries to equip a valid armor it will get that armor, and it will also
            // put the armor to the mages hashmap and set armor attribute
            this.armor = armor;
            setTotalAttributes();
            putSlotsAndArmorInHashmap();
            return true;
        }
    }
    //This sets the heroDBS. It's calculated with the WeaponDPS which is attackSpeed * damage
    //All heroes have a main primary attribute, for mage it's intelligence
    @Override
    public Double getHeroDPS() {
        if (getWeapon() == null) {
            return setHeroDBS(1.0) * (1 + this.getPrimaryAttributes().getIntelligence() / 100d);
        }
        return setHeroDBS((int) getWeapon().getWeaponDPS() *
                (1 + this.getPrimaryAttributes().getIntelligence() / 100d));
    }

    //This basically fix so when a mage levels up, the level should add with 1, the strength adds with 1
    //, dexterity adds with one and intelligence adds with 5. Then it returns the new value
    public PrimaryAttributes levelUp() {
        setLevel(getLevel() + 1);
        getPrimaryAttributes().setStrength(getPrimaryAttributes().getStrength() + 1);
        getPrimaryAttributes().setDexterity(getPrimaryAttributes().getDexterity() + 1);
        getPrimaryAttributes().setIntelligence(getPrimaryAttributes().getIntelligence() + 5);
        return getPrimaryAttributes();
    }


}
