package Heros;

import Exceptions.InvalidArmorException;
import Exceptions.InvalidSlotException;
import Exceptions.InvalidWeaponException;
import Items.*;

public class Rouge extends Hero {

    private Weapon weapon;
    private Armor armor;

    public Rouge(String name) {
        super(name, 1, new PrimaryAttributes(2, 6, 1));
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public Armor getArmor() {
        return armor;
    }

    //Sets a weapon for the rouge, but first it must pass all if statements
    @Override
    public Boolean setWeapon(Weapon weapon) throws Exception {
        //Declaring some variables for the user input
        WeaponType weaponType = weapon.getWeaponType();
        Slots slots = weapon.getSlots();
        int weaponLevel = weapon.getLevel();
        //a rouge can only equip a DAGGER or SWORD as weapon. If it tries to equip another
        // weapon than that it throws InvalidWeaponException
        if (!(weaponType.equals(WeaponType.DAGGER) ||
                weaponType.equals(WeaponType.SWORD))) {
            throw new InvalidWeaponException("You can not get that weapon");
        }
        //If rouge tries to equip weapon with higher level than the rouge has it will throw
        // InvalidWeaponException
        if (weaponLevel > getLevel()) {
            throw new InvalidWeaponException("You can't get a weapon with that level");
        }
        //If the rouge tries to enter a slot that isn't weapon it throws InvalidSlotException
        if (!slots.equals(Slots.WEAPON)) {
            throw new InvalidSlotException("You must put WEAPON as slot");
        } else {
            //If rouge try to set weapon with right slot, weaponType and level it will get that
            //Weapon
            // ,and it will also put the weapon in rouges hashmap
            this.weapon = weapon;
            putSlotsAndWeaponInHashmap();
            return true;
        }
    }

    //Sets armor for the Rouge, but first it must pass all if statements
    @Override
    public Boolean setArmor(Armor armor) throws InvalidArmorException, InvalidSlotException {
        //Declaring some variables for the user input
        ArmorType armorType = armor.getArmorType();
        int armorLevel = armor.getLevel();
        Slots slots = armor.getSlots();
        //Rouge can only equip armor with type leather or mail, so if rouge tries to equip
        //another type than that it will throw InvalidArmorException
        //Armor can't be put in weapon slot, if hero tries to do it exception will throw
        if(slots.equals(Slots.WEAPON)){
            throw new InvalidSlotException("You can't put armor in WEAPON slot!");
        }
        if (!(armorType.equals(ArmorType.LEATHER) ||
                armorType.equals(ArmorType.MAIL))) {
            throw new InvalidArmorException("You can not get that armor");
        }
        //If rouge tries to equip armor with higher level than the rouge has it will throw
        //InvalidArmorException
        if (armorLevel > getLevel()) {
            throw new InvalidArmorException("You can't get armor with that level");
        } else {
            //If rouge tries to equip a valid armor it will get that armor, and it will also
            // put the armor to the rouges hashmap and set armor attribute
            this.armor = armor;
            setTotalAttributes();
            putSlotsAndArmorInHashmap();
            return true;
        }
    }
    //This sets the heroDBS. It's calculated with the WeaponDPS which is attackSpeed * damage * main primary attribute
    //All heroes have a main primary attribute, for rouge it's the dexterity
    @Override
    public Double getHeroDPS() {
        if (getWeapon() == null) {
            return setHeroDBS(1.0) * (1 + this.getPrimaryAttributes().getDexterity() / 100d);
        }
        return setHeroDBS((int) getWeapon().getWeaponDPS() *
                (1 + this.getPrimaryAttributes().getDexterity() / 100d));
    }
    //This basically fix so when a rouge levels up, the level should add with 1, the strength adds with 1
    //, dexterity adds with 4 and intelligence adds with 1. Then it returns the new value
    @Override
    public PrimaryAttributes levelUp() {
        setLevel(getLevel() + 1);
        getPrimaryAttributes().setStrength(getPrimaryAttributes().getStrength() + 1);
        getPrimaryAttributes().setDexterity(getPrimaryAttributes().getDexterity() + 4);
        getPrimaryAttributes().setIntelligence(getPrimaryAttributes().getIntelligence() + 1);
        return getPrimaryAttributes();
    }
}
