package Heros;

import Exceptions.InvalidArmorException;
import Exceptions.InvalidSlotException;
import Exceptions.InvalidWeaponException;
import Items.*;

public class Warrior extends Hero {

    private Weapon weapon;
    private Armor armor;

    public Warrior(String name) {
        super(name, 1,  new PrimaryAttributes(5, 2, 1));
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public Armor getArmor() {
        return armor;
    }

    //Sets a weapon for the warrior, but first it must pass all if statements
    @Override
    public Boolean setWeapon(Weapon weapon) throws Exception {
        //Declaring some variables for the user input
        WeaponType weaponType = weapon.getWeaponType();
        int weaponLevel = weapon.getLevel();
        Slots slots = weapon.getSlots();
        //a warrior can only equip a AXE, HAMMER or SWORD as weapon. If it tries to equip another
        // weapon than that it throws InvalidWeaponException
        if (!(weaponType.equals(WeaponType.AXE) ||
                weaponType.equals(WeaponType.HAMMER) ||
                weaponType.equals(WeaponType.SWORD))) {
            throw new InvalidWeaponException("You can not get that weapon type");
        }
        //If warrior tries to equip weapon with higher level than the warrior has it will throw
        // InvalidWeaponException
        if (weaponLevel > getLevel()) {
            throw new InvalidWeaponException("You can't get a weapon with that level");
        }
        //If the warrior tries to enter a slot that isn't weapon it throws InvalidSlotException
        if (!slots.equals(Slots.WEAPON)) {
            throw new InvalidSlotException("You must put WEAPON as slot");
        } else {
            //If the warrior try to set weapon with right slot, weaponType and level it will get that
            //Weapon
            // ,and it will also put the weapon in warriors hashmap
            this.weapon = weapon;
            putSlotsAndWeaponInHashmap();
            return true;
        }
    }

    //Sets armor for the Warrior, but first it must pass all if statements
    @Override
    public Boolean setArmor(Armor armor) throws InvalidArmorException, InvalidSlotException {
        //Declaring some variables for the user input
        ArmorType armorType = armor.getArmorType();
        int armorLevel = armor.getLevel();
        Slots slots = armor.getSlots();
        //Warrior can only equip armor with type plate or mail, so if warrior tries to equip
        //another type than that it will throw InvalidArmorException
        //Armor can't be put in weapon slot, if hero tries to do it exception will throw
        if(slots.equals(Slots.WEAPON)){
            throw new InvalidSlotException("You can't put armor in WEAPON slot!");
        }
        if (!(armorType.equals(ArmorType.MAIL) ||
                armorType.equals(ArmorType.PLATE))) {
            throw new InvalidArmorException("You can not get that armor typ");
        }
        //If warrior tries to equip armor with higher level than the warrior has it will throw
        //InvalidArmorException
        if (armorLevel > getLevel()) {
            throw new InvalidArmorException("You can't have armor with that level");
        } else {
            //If warrior tries to equip a valid armor it will get that armor, and it will also
            // put the armor to the rangers hashmap and set armor attribute
            this.armor = armor;
            setTotalAttributes();
            putSlotsAndArmorInHashmap();
            return true;
        }
    }
    //This sets the heroDBS. It's calculated with the WeaponDPS which is attackSpeed * damage
    //All heroes have a main primary attribute, for warrior it's the strength
    @Override
    public Double getHeroDPS() {
        if (getWeapon() == null) {
            return setHeroDBS(1.0) * (1 + this.getPrimaryAttributes().getStrength() / 100d);
        }
        return setHeroDBS((int) (getWeapon().getWeaponDPS()) *
                (1 + this.getPrimaryAttributes().getStrength() / 100d));
    }

    //This basically fix so when a warrior levels up, the level should add with 1, the strength adds with 3
    //, dexterity adds with 2 and intelligence adds with 1. Then it returns the new value
    @Override
    public PrimaryAttributes levelUp() {
        setLevel(getLevel() + 1);
        getPrimaryAttributes().setStrength((getPrimaryAttributes().getStrength() + 3));
        getPrimaryAttributes().setDexterity(getPrimaryAttributes().getDexterity() + 2);
        getPrimaryAttributes().setIntelligence(getPrimaryAttributes().getIntelligence() + 1);
        return getPrimaryAttributes();
    }
}
