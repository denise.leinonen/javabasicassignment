package Heros;

import Items.*;

import java.util.HashMap;

public abstract class Hero {
    private String name;
    private int level;
    private PrimaryAttributes primaryAttributes;
    private Armor armor;
    private Weapon weapon;
    HashMap<Slots, Item> slotsMaps;
    private double heroDPS;

    public Hero(String name, int level,PrimaryAttributes primaryAttributes) {
        this.name = name;
        this.level = level;
        this.primaryAttributes = primaryAttributes;
        this.slotsMaps = new HashMap<>();
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public Armor getArmor() {
        return armor;
    }

    public Double getHeroDPS() {
        return heroDPS;
    }

    public PrimaryAttributes getPrimaryAttributes() {
        return primaryAttributes;
    }

    public HashMap<Slots, Item> getSlotsMaps() {
        return slotsMaps;
    }

    public abstract Boolean setWeapon(Weapon weapon)throws Exception;

    public abstract Boolean setArmor(Armor armor) throws Exception;

    public int setLevel(int level) {
        this.level = level;
        return level;
    }

    public double setHeroDBS(double heroDBS) {
        this.heroDPS = heroDBS;
        return heroDBS;
    }
    //This method first get the slot and armor that the user wrote in, then it will get hero hashmap and
    //put the slot as key and armor as value. Once equipped armor this
    //method is called
    public void putSlotsAndArmorInHashmap() {
        Slots slots = getArmor().getSlots();
        getSlotsMaps().put(slots, getArmor());
    }

    //This method gets the slot and weapon hero want to set, and get heros hashmap.
    //then it put the slot as a key and weapon as value, once equipped weapon this
    //method is called
    public void putSlotsAndWeaponInHashmap() {
        Slots slots = getWeapon().getSlots();
        getSlotsMaps().put(slots, getWeapon());
    }

    //Sets a basic level up function for a hero, every hero has their own level up attributes that is set
    //in overwritten function
    public PrimaryAttributes levelUp() {
        this.level++;
        getPrimaryAttributes().setDexterity(primaryAttributes.getDexterity() + 1);
        getPrimaryAttributes().setStrength(primaryAttributes.getStrength() + 1);
        getPrimaryAttributes().setIntelligence(primaryAttributes.getIntelligence() + 1);
        return primaryAttributes;
    }

    //This calculates the totalAttribute for hero. PrimaryAttributes are added with armorAttributes

    public PrimaryAttributes setTotalAttributes() {
        int armorDexterity = getArmor().getPrimaryAttributes().getDexterity();
        int armorStrength = getArmor().getPrimaryAttributes().getStrength();
        int armorIntelligence = getArmor().getPrimaryAttributes().getIntelligence();

        getPrimaryAttributes().setStrength(getPrimaryAttributes().getStrength() + armorStrength);
        getPrimaryAttributes().setDexterity(getPrimaryAttributes().getDexterity() + armorDexterity);
        getPrimaryAttributes().setIntelligence(getPrimaryAttributes().getIntelligence() + armorIntelligence);
        return getPrimaryAttributes();
    }

    @Override
    public String toString() {
        return "Hero " + '\n' +
                "name: " + name + '\n' +
                "level: " + level + '\n' +
                 primaryAttributes + '\n' +
                "heroDPS: " + getHeroDPS();
    }
}
