package Exceptions;

public class InvalidSlotException extends Exception{

    public InvalidSlotException(String errorMessage){
        super(errorMessage);
    }
}
