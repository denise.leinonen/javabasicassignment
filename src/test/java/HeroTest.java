
import Exceptions.InvalidSlotException;
import Heros.Mage;
import Heros.Ranger;
import Heros.Rouge;
import Heros.Warrior;
import Items.Slots;
import Items.Weapon;
import Items.WeaponType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class HeroTest {

    private Mage mage;
    private Warrior warrior;
    private Rouge rouge;
    private Ranger ranger;
    @BeforeEach
    void setUp() {
        mage = new Mage("Heros.Mage");
        warrior = new Warrior("Heros.Warrior");
        rouge = new Rouge("Heros.Rouge");
        ranger = new Ranger("Heros.Ranger");
    }
    @Test
    void test_create_mage_level_should_be_1_succeed(){
        Mage mage1 = new Mage("Heros.Mage");
        int expectedLevel = 1;

        Assertions.assertEquals(mage1.getLevel(), expectedLevel);
    }
    @Test
    void test_when_mage_levels_up_level_should_be_2_succeed(){
        int expected = 2;

        mage.levelUp();

        Assertions.assertEquals(mage.getLevel(), expected);
    }

    @Test
    void test_when_create_mage_default_primary_attributes_should_be_set(){
        int expectedStrength = 1;
        int expectedDexterity = 1;
        int expectedIntelligence = 8;

        Assertions.assertEquals(mage.getPrimaryAttributes().getStrength(), expectedStrength);
        Assertions.assertEquals(mage.getPrimaryAttributes().getDexterity(), expectedDexterity);
        Assertions.assertEquals(mage.getPrimaryAttributes().getIntelligence(), expectedIntelligence);
    }

    @Test
    void test_when_create_warrior_default_primary_attributes_should_be_set(){
        int expectedStrength = 5;
        int expectedDexterity = 2;
        int expectedIntelligence = 1;

        Assertions.assertEquals(warrior.getPrimaryAttributes().getStrength(),expectedStrength);
        Assertions.assertEquals(warrior.getPrimaryAttributes().getDexterity(),expectedDexterity);
        Assertions.assertEquals(warrior.getPrimaryAttributes().getIntelligence(),expectedIntelligence);
    }

    @Test
    void test_when_create_ranger_default_primary_attributes_should_be_set(){
        int expectedStrength = 1;
        int expectedDexterity = 1;
        int expectedIntelligence = 7;

        Assertions.assertEquals(ranger.getPrimaryAttributes().getStrength(),expectedStrength);
        Assertions.assertEquals(ranger.getPrimaryAttributes().getDexterity(),expectedDexterity);
        Assertions.assertEquals(ranger.getPrimaryAttributes().getIntelligence(),expectedIntelligence);
    }

    @Test
    void test_when_create_rouge_default_primary_attributes_should_be_set(){
        int expectedStrength = 2;
        int expectedDexterity = 6;
        int expectedIntelligence = 1;

        Assertions.assertEquals(rouge.getPrimaryAttributes().getStrength(),expectedStrength);
        Assertions.assertEquals(rouge.getPrimaryAttributes().getDexterity(),expectedDexterity);
        Assertions.assertEquals(rouge.getPrimaryAttributes().getIntelligence(),expectedIntelligence);
    }

    @Test
    void test_when_mage_level_up_strength_should_increase_with_right_values(){
        int expectedStrength = 2;

        mage.levelUp();

        Assertions.assertEquals(mage.getPrimaryAttributes().getStrength(), expectedStrength);
    }
    @Test
    void test_when_mage_level_up_dexterity_should_increase_with_right_value(){
        int expectedDexterity = 2;

        mage.levelUp();

        Assertions.assertEquals(mage.getPrimaryAttributes().getDexterity(), expectedDexterity);
    }
    @Test
    void test_when_mage_level_up_intelligence_should_increase_with_right_value(){
        int expectedIntelligence = 13;

        mage.levelUp();

        Assertions.assertEquals(mage.getPrimaryAttributes().getIntelligence(), expectedIntelligence);
    }


    @Test
    void test_when_warrior_level_up_strength_should_increase_with_right_value(){
        int expectedStrength = 8;

        warrior.levelUp();

        Assertions.assertEquals(warrior.getPrimaryAttributes().getStrength(),expectedStrength);
    }
    @Test
    void test_when_warrior_level_up_dexterity_should_increase_with_right_value(){
        int expectedDexterity = 4;

        warrior.levelUp();

        Assertions.assertEquals(warrior.getPrimaryAttributes().getDexterity(),expectedDexterity);
    }
    @Test
    void test_when_warrior_level_up_intelligence_should_increase_with_right_value(){
        int expectedIntelligence = 2;

        warrior.levelUp();

        Assertions.assertEquals(warrior.getPrimaryAttributes().getIntelligence(),expectedIntelligence);
    }
    @Test
    void test_when_ranger_level_up_strength_should_increase_with_right_value(){
        int expectedStrength = 2;

        ranger.levelUp();

        Assertions.assertEquals(ranger.getPrimaryAttributes().getStrength(),expectedStrength);
    }
    @Test
    void test_when_ranger_level_up_dexterity_should_increase_with_right_value(){
        int expectedDexterity = 6;

        ranger.levelUp();

        Assertions.assertEquals(ranger.getPrimaryAttributes().getDexterity(),expectedDexterity);
    }
    @Test
    void test_when_ranger_level_up_intelligence_should_increase_with_right_value(){
        int expectedIntelligence = 8;

        ranger.levelUp();

        Assertions.assertEquals(ranger.getPrimaryAttributes().getIntelligence(),expectedIntelligence);
    }
    @Test
    void test_when_rouge_level_up_strength_should_increase_with_right_value(){
        int expectedStrength = 3;

        rouge.levelUp();

        Assertions.assertEquals(rouge.getPrimaryAttributes().getStrength(),expectedStrength);
    }
    @Test
    void test_when_rouge_level_up_dexterity_should_increase_with_right_value(){
        int expectedDexterity = 10;

        rouge.levelUp();

        Assertions.assertEquals(rouge.getPrimaryAttributes().getDexterity(),expectedDexterity);
    }
    @Test
    void test_when_rouge_level_up_intelligence_should_increase_with_right_value(){
        int expectedIntelligence = 2;

        rouge.levelUp();

        Assertions.assertEquals(rouge.getPrimaryAttributes().getIntelligence(),expectedIntelligence);
    }

    @Test
    void test_try_to_set_weapon_slot_as_wrong_slot_should_throw_exception(){
        InvalidSlotException thrown = Assertions.assertThrows(
                InvalidSlotException.class, () -> {
                    mage.setWeapon(new Weapon("Default", 1, Slots.HEAD, 2, 2, WeaponType.STAFF));
                    rouge.setWeapon(new Weapon("cool weapon", 1, Slots.LEGS, 4, 2, WeaponType.DAGGER));
                    warrior.setWeapon(new Weapon("default", 1, Slots.HEAD, 4, 2, WeaponType.SWORD));
                    ranger.setWeapon(new Weapon("default", 1, Slots.BODY, 4, 5, WeaponType.BOW));
                }
        );
        Assertions.assertTrue(thrown.getMessage().contains("You must put WEAPON as slot"));
    }


}