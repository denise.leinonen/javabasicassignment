import Exceptions.InvalidArmorException;
import Exceptions.InvalidSlotException;
import Exceptions.InvalidWeaponException;
import Heros.Mage;
import Heros.Warrior;
import Items.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.DecimalFormat;

class ItemTest {

    @BeforeEach
    void setUp() {
    }

    @Test
    void test_warrior_cant_have_that_weapon_wrong_level_should_throw_exception() {
        InvalidWeaponException thrown = Assertions.assertThrows(
                InvalidWeaponException.class, () -> {
                    Warrior warrior = new Warrior("Heros.Warrior");
                    warrior.setWeapon(new Weapon("Default", 2, Slots.WEAPON, 2, 2, WeaponType.AXE));
                }
        );
        Assertions.assertTrue(thrown.getMessage().contains("You can't get a weapon with that level"));
    }

    @Test
    void test_warrior_cant_have_that_armor_wrong_level_should_throw_exception() {
        InvalidArmorException thrown = Assertions.assertThrows(
                InvalidArmorException.class, () -> {
                    Warrior warrior = new Warrior("Heros.Warrior");
                    warrior.setArmor(new Armor("Default", 2, Slots.HEAD, ArmorType.PLATE, new PrimaryAttributes(2, 4, 5)));
                }
        );
        Assertions.assertTrue(thrown.getMessage().contains("You can't have armor with that level"));
    }

    @Test
    void test_warrior_cant_have_that_weapon_wrong_weapon_type_should_throw_exception() {
        InvalidWeaponException thrown = Assertions.assertThrows(
                InvalidWeaponException.class, () -> {
                    Warrior warrior = new Warrior("Heros.Warrior");
                    warrior.setWeapon(new Weapon("Default", 1, Slots.WEAPON, 2, 2, WeaponType.BOW));
                }
        );
        Assertions.assertTrue(thrown.getMessage().contains("You can not get that weapon"));
    }

    @Test
    void test_warrior_cant_have_that_armor_wrong_armor_type_should_throw_exception() {
        InvalidArmorException thrown = Assertions.assertThrows(
                InvalidArmorException.class, () -> {
                    Warrior warrior = new Warrior("Heros.Warrior");
                    warrior.setArmor(new Armor("Items.Armor", 2, Slots.HEAD, ArmorType.CLOTH, new PrimaryAttributes(2, 3, 4)));
                }
        );
        Assertions.assertTrue(thrown.getMessage().contains("You can not get that armor typ"));
    }

    @Test
    void test_warrior_should_get_right_weapon_result_should_be_true() throws Exception {
        Warrior warrior = new Warrior("Warrior");
        Boolean weapon = warrior.setWeapon(new Weapon("Cool weapon", 1, Slots.WEAPON, 2, 2, WeaponType.HAMMER));

        Assertions.assertTrue(weapon);
    }

    @Test
    void test_warrior_should_get_right_armor_result_should_be_true() throws Exception {
        Warrior warrior = new Warrior("Warrior");
        Boolean armor = warrior.setArmor(new Armor("Armor", 1, Slots.HEAD, ArmorType.MAIL, new PrimaryAttributes(1, 5, 3)));

        Assertions.assertTrue(armor);
    }

    @Test
    void test_warrior_equip_armor_should_get_right_strength_attribute() throws InvalidArmorException, InvalidSlotException {
        Warrior warrior = new Warrior("Cool guy");

        warrior.setArmor(new Armor("the best", 1, Slots.BODY, ArmorType.PLATE, new PrimaryAttributes(2, 4, 2)));

        Assertions.assertEquals(warrior.getPrimaryAttributes().getStrength(), 7);
    }

    @Test
    void test_warrior_equip_armor_should_get_right_dexterity_attribute() throws InvalidArmorException, InvalidSlotException {
        Warrior warrior = new Warrior("Cool guy");

        warrior.setArmor(new Armor("the best", 1, Slots.BODY, ArmorType.PLATE, new PrimaryAttributes(2, 2, 5)));

        Assertions.assertEquals(warrior.getPrimaryAttributes().getDexterity(), 4);
    }

    @Test
    void test_warrior_equip_armor_should_get_right_intelligence_attribute() throws InvalidArmorException, InvalidSlotException {
        Warrior warrior = new Warrior("Cool guy");

        warrior.setArmor(new Armor("the best", 1, Slots.BODY, ArmorType.PLATE, new PrimaryAttributes(2, 5, 1)));

        Assertions.assertEquals(warrior.getPrimaryAttributes().getIntelligence(), 2);
    }

    @Test
    void test_warrior_DPS_without_equipped_weapon_should_be_correct() {
        Warrior warrior = new Warrior("Nisan");
        Assertions.assertEquals(warrior.getHeroDPS(), 1.05);
    }

    @Test
    void test_warrior_DPS_with_weapon_should_be_correct() throws Exception {
        Warrior warrior = new Warrior("Nisan");
        warrior.setWeapon(new Weapon("cool axe", 1, Slots.WEAPON, 2, 3.5, WeaponType.AXE));
        DecimalFormat df = new DecimalFormat("#.##");
        String expectedDPS = "7,35";

        Assertions.assertEquals(df.format(warrior.getHeroDPS()), expectedDPS);
        System.out.println(warrior.getHeroDPS());
    }

    @Test
    void test_warrior_DPS_with_armor_and_weapon_should_be_correct() throws Exception {
        Warrior warrior = new Warrior("Bad guy");
        warrior.setArmor(new Armor("Cool", 1, Slots.BODY, ArmorType.PLATE, new PrimaryAttributes(2, 4, 5)));
        warrior.setWeapon(new Weapon("very dangerous", 1, Slots.WEAPON, 2, 3.5, WeaponType.AXE));
        DecimalFormat df = new DecimalFormat("#.##");
        String expectedDPS = "7,49";

        Assertions.assertEquals(df.format(warrior.getHeroDPS()), expectedDPS);
    }

    @Test
    void test_should_create_armor_armor_type_should_be_cloth() {
        Armor armor = new Armor("Sweet armor", 4, Slots.BODY, ArmorType.CLOTH, new PrimaryAttributes(2, 5 , 4));

        Assertions.assertEquals(armor.getArmorType(), ArmorType.CLOTH);
    }

    @Test
    void test_should_create_armor_armor_type_should_be_leather() {
        Armor leather = new Armor("Great Leather", 4, Slots.LEGS, ArmorType.LEATHER, new PrimaryAttributes(1, 5, 4));

        Assertions.assertEquals(leather.getArmorType(), ArmorType.LEATHER);
    }

    @Test
    void test_should_create_armor_armor_type_should_be_plate() {
        Armor armor = new Armor("very expensive one", 4, Slots.HEAD, ArmorType.PLATE, new PrimaryAttributes(2, 4, 5));

        Assertions.assertEquals(armor.getArmorType(), ArmorType.PLATE);
    }

    @Test
    void test_should_create_armor_armor_type_should_be_mail() {
        Armor armor = new Armor("the best", 3, Slots.LEGS, ArmorType.MAIL, new PrimaryAttributes(1, 4, 5));

        Assertions.assertEquals(armor.getArmorType(), ArmorType.MAIL);
    }

    @Test
    void test_create_new_weapon_should_get_the_right_DPS_for_the_weapon() throws Exception {
        Mage mage = new Mage("Denise");
        mage.setWeapon(new Weapon("default", 1, Slots.WEAPON, 3, 3, WeaponType.STAFF));

        Assertions.assertEquals(mage.getWeapon().getWeaponDPS(), 9);
    }

    @Test
    void test_when_warrior_equip_weapon_it_should_be_stored_in_hashmap_with_WEAPON_key() throws Exception {
        Warrior warrior = new Warrior("Warrior");
        warrior.setWeapon(new Weapon("Weapon", 1, Slots.WEAPON, 2, 3, WeaponType.SWORD));

        Assertions.assertTrue(warrior.getSlotsMaps().containsKey(Slots.WEAPON));
    }

    @Test
    void test_when_warrior_equip_weapon_it_should_be_stored_in_hashmap_with_right_value() throws Exception {
        Warrior warrior = new Warrior("Warrior");
        warrior.setWeapon(new Weapon("Weapon", 1, Slots.WEAPON, 2, 3, WeaponType.SWORD));

        Assertions.assertTrue(warrior.getSlotsMaps().containsValue(warrior.getWeapon()));
    }

    @Test
    void test_when_warrior_equip_armor_it_should_be_stored_in_hashmap_with_right_sloth_key() throws Exception {
        Warrior warrior = new Warrior("Warrior");
        warrior.setArmor(new Armor("cool armor", 1, Slots.BODY, ArmorType.PLATE, new PrimaryAttributes(1, 3, 5)));

        Assertions.assertTrue(warrior.getSlotsMaps().containsKey(Slots.BODY));
    }

    @Test
    void test_when_warrior_equip_armor_it_should_be_stored_in_hashmap_with_right_value() throws Exception {
        Warrior warrior = new Warrior("Warrior");
        warrior.setArmor(new Armor("cool armor", 1, Slots.BODY, ArmorType.PLATE, new PrimaryAttributes(1, 3, 4)));

        Assertions.assertTrue(warrior.getSlotsMaps().containsValue(warrior.getArmor()));
    }

    @Test
    void test_when_warrior_tries_to_store_armor_in_weapon_slot_it_should_throw_exception() {
        InvalidSlotException thrown = Assertions.assertThrows(
                InvalidSlotException.class, () -> {
                    Warrior warrior = new Warrior("Heros.Warrior");
                    warrior.setArmor(new Armor("Items.Armor", 2, Slots.WEAPON, ArmorType.CLOTH, new PrimaryAttributes(1, 4, 5)));
                }
        );
        Assertions.assertTrue(thrown.getMessage().contains("You can't put armor in WEAPON slot!"));
    }

}